# scrollbaJS Readme #


This tiny jQuery plugin will show you a reading or scroll position indicator on the left side of your webpage.

### Who is behind? ###

* Ludwig Jäschke
* Whydesign x Webentwicklung
* [www.whydesign-halle.de](http://whydesign-halle.de)
* [info@whydesign-halle.de](mailto:info@whydesign-halle.de)

### How do I set up? ###

For scrollbaJS you will need a stable version of the [jQuery Lib](https://jquery.com/).

I used in the demo the jQuery v1.12.0 - (c) jQuery Foundation | jquery.org/license

### 1. Download and include the JS in the *<head>* of your site: ###


```
#!html

<script src="jquery.min.js"></script>
<script src="scrollba.min.js"></script>
```


### 2. Add the *scrollbaJS* function: ###


```
#!javascript

$(document).on('ready', function() {
   scrollba(color, width, shadow)
});
```


### 3. Define the *scrollbaJS* style options: ###


```
#!html

color = HEX Color Code (#545454)
width = Width in px (5px)
shadow = true or false

scrollba('#545454', '5px', true)
```