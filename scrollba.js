////////////////////////////////////////////////////////
//// scrollbaJS - jQuery Reading Position Indicator
//// Plugin Version 0.1
//// https://bitbucket.org/ludwig_jaeschke
////////////////////////////////////////////////////////
//// Ludwig Jäschke - Whydesign X Webentwicklung
//// www.whydesign-halle.de
//// info@whydesign-halle.de
////////////////////////////////////////////////////////

function scrollba(color, width, shadow) {

	if (shadow == true) {
		var scrollbaShadow = 'box-shadow: 0 0 7px rgba(0, 0, 0, 0.5);';
	}
	else {
		var scrollbaShadow = 'box-shadow: none;';
	}

	var scrollbaStyle = 'width:' + width + '; background:' + color + '; ' + scrollbaShadow + ' position: fixed; top: 0; border-bottom-right-radius: 2px; z-index: 99999;';

	$('body').append('<div class="scrollba" style="' + scrollbaStyle + '"></div>');

	var winHeight = $(window).height(), 
		docHeight = $(document).height(),
		progressBar = $('.scrollba'),
		max, value, divHeight;

	max = docHeight - winHeight;

    progressBar.css({'max-height': winHeight + 'px'});

    $(document).on('scroll', function(){

        value = $(window).scrollTop();

        divHeight = value * 100 / max;

		progressBar.css({'height': divHeight + '%'});
	});

}